<?php

return [


    'label'=>[
        'users'=>'משתמשים',
        'user'=>'משתמש',
        'id'=>'תעודת זהות',
        'name'=>'שם',
        'email'=>'דוא"ל',
        'role'=>'תפקיד',
        'password'=>'Password',
        'website'=>'Website',
        'create_new_user'=>'Create New User',
        'create_user'=>'Create User',
        'edit_user'=>'Edit User',
        'show_user'=>'Show User',
        'select_user'=>'Select User',
        'accessible_website' => 'Accessible Website',
		'profile'=>'פרופיל',
		'logout'=>'להתנתק',
    ]
];