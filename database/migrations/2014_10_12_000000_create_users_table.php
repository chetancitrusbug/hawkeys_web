<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('location')->nullable();
            $table->string('city')->nullable();
            $table->string('profile_pic')->nullable();
            $table->string('social_media_id')->nullable();
            $table->string('stirpe_token')->nullable();
            $table->string('phone_number')->nullable();
            $table->enum('status', ['active','inactive','pending'])->default( 'pending');
            $table->enum('registration_type', [ 'facebook', 'google', 'simple'])->default( 'simple');
            $table->string('email');
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
