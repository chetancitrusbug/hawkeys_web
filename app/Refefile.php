<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Refefile extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'refe_file';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['refe_table_field_name', 'ref_field_id', 'refe_file_path','refe_file_name', 'ref_code','ref_type'];
	
	protected $appends = ['file_url'];
	
	public function getFileUrlAttribute()
    {
		if($this->refe_file_path && \File::exists(public_path()."/".$this->refe_file_path)){
			return url("/")."/".$this->refe_file_path;
		}else{
			return "";
		}
	}


   


}
