<?php

return [

    'roles' => [
            '0' => [
                'label' => ' Admin',
                'name' => 'AU'
            ],
            '1' => [
                'label' => 'Vendor',
                'name' => 'VN'
            ],
            '2' => [
                'label' => 'Customer',
                'name' => 'CS'
            ],
        ],
		
	'users' => [
            '0' => [
                'name' => ' Admin User',
                'first_name' => 'Admin',
                'last_name' => '',
				'status'=>'active',
				'email'=>'admin@gmail.com',
				'password'=>'123456',
				'role'=>'AU',
			],
            '1' => [
                'name' => 'Vendor User',
                'first_name' => 'Vnd',
                'last_name' => '',
				'status'=>'active',
				'email'=>'vendor@gmail.com',
				'password'=>'123456',
				'role'=>'VN',
			],
            '2' => [
                'name' => 'Customer User',
                'first_name' => 'Customer',
                'last_name' => '',
				'status'=>'active',
				'email'=>'customer@gmail.com',
				'password'=>'123456',
				'role'=>'CS',
			],
        ],	

];
