<?php

return [

	
	'responce_msg' => [
		'success_default' => 'Action Success !',
        'success_login' => 'Login Success.',
        'success_logout' => 'You have successfully logged out!',
        'success_forgot_password_send_to_email' => 'One time password (otp) to reset your password has been sent to your registered email address.',
        'success_forgot_password_send_to_phone_no' => 'One time password has been sent on your phone number.',
        'success_password_changed' => 'Success! Your Password has been changed!',
        'success_register' => 'User Register Successfully.',
        'success_profile_updated' => 'Profile Updated Successfully.',
        
		
        'warning_data_not_found' => 'User data not found .',
        'warning_no_data_found' => 'No Record Found.',
        'warning_your_account_not_activated_yet' => 'Your account has not activated Yet.',
        'warning_incorrect_email_or_password' => 'Invalid email or password',
        'warning_require_unique_phone_number' => 'Phone number already exists with another account. Try with another number',
        'warning_require_unique_user_name' => 'Username already exists. please try another one.',
        'warning_your_role_is_not_found' => 'User role not valid.',
        'user_not_found'=> 'User Not Found',
        'password_reset_mail' => 'Password reset link has been sent to your registered email',
        'user_not_active'=> 'User is not active, Please contact your admin',
        
		
		'error_default'=>'Something went wrong !',
		
        
		
		
        
		
    ],

    
	
];
